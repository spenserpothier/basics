
public class LinkedList<T> {
	private Node<T> head;
	private int len;
	
	public LinkedList(){
		this.head = null;
		this.len = 0;
	}
	
	public LinkedList(T value){
		this.head = new Node<T>(value);
		this.len = 1;
	}
	
	public void addNode(T value) {
		Node<T> n = new Node<T>(value);
		Node<T> current = this.head;
		while (current.getNext() != null) {
			current = current.getNext();
		}
		current.setNext(n);
		this.len++;
	}
	
	@Override
	public String toString() {
		Node<T> current = this.head;
		String output = "";
		while (current != null) {
			output += current.toString() + " ";
			current = current.getNext();
		}
		
		return output;
	}
}
