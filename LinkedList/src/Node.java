/**
 * Generic Node class to be used in various basic data structures
 * @author spenser
 *
 * @param <T>
 */
public class Node<T> {

	private T nodeData;
	private Node<T> nextNode;
	
	public Node() {
		this.nodeData = null;
		this.nextNode = null;
	}
	
	/**
	 * Creates a new node with initial value
	 * @param value
	 */
	public Node(T value){
		this.nodeData = value;
		this.nextNode = null;
	}
	
	/**
	 * Returns the data stored in this node
	 * @return data stored in the node
	 */
	public T getData() {
		return this.nodeData;
	}
	
	/**
	 * Sets the value for this node
	 * @param value value to set
	 */
	public void setData(T value) {
		this.nodeData = value;
	}
	
	/**
	 * Returns a "pointer" to the next node
	 * @return the next node
	 */
	public Node<T> getNext() {
		return this.nextNode;
	}
	
	/**
	 * Sets the pointer to the next node
	 * @param n node to use for the next node
	 */
	public void setNext(Node<T> n) {
		this.nextNode = n;
	}
	
	/**
	 * Returns a string representation of the node
	 */
	@Override
	public String toString(){
		return nodeData.toString();
	}
}
